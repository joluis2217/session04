package com.example.laboratorio04

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import java.util.zip.Inflater

class MainActivity : AppCompatActivity() {

    var MarcaSelecciona=""





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val marcas = listOf("BMW","Audi","KIA","Hyundai")

        val adaptadorMarca = ArrayAdapter(this,R.layout.stye_item_spinner,marcas)

        spMarca.adapter=adaptadorMarca



        spMarca.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, Index: Int, p3: Long) {

                MarcaSelecciona=marcas[Index]

            }

        }

        btnMostrar.setOnClickListener {
            val Titular=edtNombre.text.toString()
            val ano=edtAño.text.toString()
            val descripcion=edtDescripcion.text.toString()
            val Auto=Auto(Titular,MarcaSelecciona,ano,descripcion)

            if (Titular==""){
                Toast.makeText(this,"Ingrese el nombre del titular",Toast.LENGTH_LONG).show()

            }else if (ano==""){
                Toast.makeText(this,"Ingrese el año del vehiculo",Toast.LENGTH_LONG).show()
            }else if(descripcion==""){
                Toast.makeText(this,"Ingrese una descripción ",Toast.LENGTH_LONG).show()
            }else {


                CrearDialogo(Auto).show()
            }


        }






    }





    fun CrearDialogo(Auto:Auto):AlertDialog{

        var texto=""

    val alertDialog:AlertDialog

    val builder=AlertDialog.Builder(this)

    val inflater = layoutInflater

    val view:View=inflater.inflate(R.layout.dialogo,null)

    builder.setView(view)

   val contenido:TextView=view.findViewById(R.id.tvContenido)
    val btnSalir:Button=view.findViewById(R.id.btnSalir)

    texto="Estimad@  ${Auto.C_DESC_TITU}  dueño del auto :  ${Auto.C_DESC_MARC} " +
            "- ${Auto.C_ANO_AUTO} hemos  recibido su solicitud  sobre el incidente : " +
            "' ${Auto.C_DESC_PROB}' " +
            ", Gracias."

    contenido.text=texto
    alertDialog=builder.create()

    btnSalir.setOnClickListener {
        alertDialog.dismiss()


    }


    return alertDialog
    }


}
